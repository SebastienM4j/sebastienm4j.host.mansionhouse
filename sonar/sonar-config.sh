#!/bin/bash

SONAR_USERNAME=admin
SONAR_PASSWORD=admin
SONAR_HOST=localhost
SONAR_PORT=9000


isSonarRunning() {
	curl -s -u $SONAR_USERNAME:$SONAR_PASSWORD http://$SONAR_HOST:$SONAR_PORT/api/system/health | grep GREEN
	if [ $? -ne 0 ]; then
		return 1
	else
		return 0
	fi
}

tries=0
while ! isSonarRunning ; do
	if (( tries > 30 )); then
		echo "Sonar not started after 60s..."
		exit 1
	fi
	((tries++))
	sleep 2
done

echo "Sonar is started, let's configure it !"

sonarApi() {
        METHOD=$1
        shift
        URL_PATH=$1
        shift

        curl -s -u $SONAR_USERNAME:$SONAR_PASSWORD -X $METHOD http://$SONAR_HOST:$SONAR_PORT/api$URL_PATH $@

        if [ $? -eq 0 ]; then
		echo "$METHOD $URL_PATH $@"
	else
                echo "warning: request failure for $METHOD $URL_PATH $@"
        fi
}

sonarApi POST /settings/set -d 'key=sonar.webhooks.global&fieldValues={"name":"jenkins","url":"http://jenkins:8080/sonarqube-webhook/"}'

