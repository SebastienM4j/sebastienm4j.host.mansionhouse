Mansion House
=============

Usine logicielle en local (directement sur le poste de développement).


Utilisation
-----------

### Prérequis

* [Docker](https://www.docker.com/)
* [Docker Compose](https://docs.docker.com/compose/)

### Démarrer/Arrêter l'environnement

Se placer dans le répertoire du projet.

`docker-compose up` pour démarrer l'environnement.

`docker-compose down` pour arrêter l'environnement.

### Détruire l'environnement

Lorsque l'environnement est en fonctionnement, se placer dans le répertoire du projet, puis

```shell
docker-compose down --rmi all -v --remove-orphans
```
